package com.hust.hui.quicksilver.spi.api;

import com.hust.hui.quicksilver.spi.selector.DefaultSelector;
import com.hust.hui.quicksilver.spi.selector.api.ISelector;

import java.lang.annotation.*;

/**
 * Created by yihui on 2017/5/22.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Spi {
    Class<? extends ISelector> selector() default DefaultSelector.class;
}
